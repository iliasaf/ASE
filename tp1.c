#include <stdio.h>
#include <setjmp.h>
#include <assert.h>

#define CTX 0xBABE


typedef int (func_t)(int);

jump_buf buffer;
struct ctx_s *pctx;

struct ctx_s{
  void * ctx_esp;
  void * ctx_ebp;
  unsigned ctx;
};

//la fonction display
void display(){
  int i = 0;
  __asm__("movl %%esp,%0\n"
	  :"=r"(i));
  printf("%esp = %d\n",i);

  __asm__("movl %%ebp,%0\n"
	  :"=r"(i));
  printf("%ebp = %d\n",i);
}

  //la fonction try
  int try(struct ctx_s *pctx, func_t *f, int arg){
  __asm__ ("movl %%esp, %0\n"
    :"=r"(pctx->ctx_esp));
  
  __asm__ ("movl %%ebp, %0\n"
    :"=r"(pctx->ctx_ebp));
  pctx->ctx = CTX;
  return (*f)(arg);
}

  //la fonction throw
  int throw(struct ctx_s *pctx, int r){
  static int ret = 0;
  ret = r;
  assert(pctx->ctx == CTX);
	
  __asm__ ("movl %0, %%esp\n"
    :"r"(pctx->ctx_esp));
	
  __asm__ ("movl %0, %%ebp\n"
    :"r"(pctx->ctx_ebp));
  return ret;
}

  //la fonction mul
  int mul(int depth){
  int i;
  if(depth == 0){
  if(setjmp(buffer))
    throw(pctx,0);
}
  switch (scanf("%d",&i)){
 case EOF 	: 	return 1;
 case 0		:	return mul(depth+1);
 case 1		:	if (i) 	{ 
  return i * mul(depth+1);
} 
  else 	{ 
  longjmp(buffer, ~0); 
}
}

  int main(){
  int i;
  display();
  pctx = (struct ctx_s*) malloc(sizeof(struct ctx_s));
  i = try(pctx, &mul, 0);
  printf("i = %d\n",i);
  return 0;
}

	
